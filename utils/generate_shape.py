import csv

import shapely.geometry


def generate_polygon():
    """Genera un listado de polígonos a partirde su bbox"""
    with open('output.csv', 'r', newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter="\t")
        for row in reader:
            bbox = row["bbox"]
            bbox = bbox.split(',')
            bbox_float = []
            for element in bbox:
                element = float(element)
                bbox_float.append(element)
            polygon = shapely.geometry.box(*bbox_float, ccw=True)
            print(polygon.wkt)


generate_polygon()
