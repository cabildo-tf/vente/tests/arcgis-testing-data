import csv
import random

import pyproj
from shapely.ops import transform
from shapely_geojson import Feature, FeatureCollection, dump
from shapely.geometry import Point, Polygon


def calculate_extension(results, scales, imageDPI, bboxsr, imagesr, imageSize, format, transparent, layers, f, service_url):
    """Calcula la extensión alrededor de un punto específico.

           :parameters:
           results -- Listado de puntos
           scales -- Listado de escalas posibles
           imageDPI -- DPI de la imágen a generar
           bboxsr -- Referencia espacial del bbox
           imagesr -- Referencia espacial de la imagen
           imageSize -- Listado con tamaños de imágen posibles
           format -- Formato de la imágen
           transparent -- Transparencia de la imágen
           layers -- Capas de la imágen
           f -- Imagen
           """
    results_updated = []
    for result in results:
        result_updated = {}
        row_image_dpi = float(random.choice(imageDPI))
        row_image_scale = random.choice(scales)
        row_format = random.choice(format)
        row_image_size_width = random.choice(imageSize)
        row_image_size_height = random.choice(imageSize)
        # Get the map height and width for the image size and scale
        map_width_half = (((float(row_image_size_width) / float(row_image_dpi)) * 0.0254) * float(row_image_scale)) / 2
        map_height_half = (((float(row_image_size_height) / float(row_image_dpi)) * 0.0254) * float(row_image_scale)) / 2
        result_updated["bbox"] = ','.join([str(result["x"] + map_width_half), str(result["y"] + map_height_half),
                                           str(result["x"] - map_width_half), str(result["y"] - map_height_half)])
        result_updated["bboxSR"] = bboxsr
        result_updated["imageSR"] = imagesr
        result_updated["size"] = row_image_size_width + "," + row_image_size_height
        result_updated["dpi"] = row_image_dpi
        result_updated["format"] = row_format
        result_updated["transparent"] = set_transparent(transparent, row_format)
        result_updated["layers"] = layers
        result_updated["f"] = f
        result_updated["Content-Type"] = set_content_type(f, row_format)
        result_updated["serviceurl"] = service_url
        results_updated.append(result_updated)
    return results_updated


def set_transparent(transparent, format):
    """Asigna la transparencia de la imágen, de forma aleatoria si es png a falso en cualquier otro caso.

           Parámetros:
           transparent -- Listado de valores de transparencia posibles
           format -- Formato de imágen
           """
    if format == "png" or format == "png24" or format == "png32":
        return random.choice(transparent)
    else:
        return transparent[1]


def set_content_type(f, format):
    """Asigna el tipo de contenido en función del formato de imágen a utilizar.

               Parámetros:
               f -- Valor image
               format -- Formato de imágen
               """
    if format == "png" or format == "png24" or format == "png32":
        return f + "/" + "png"
    if format == "jpg":
        return f + "/" + "jpeg"
    if format == "emf":
        return "application/emf"
    if format == "ps":
        return "application/postscript"
    if format == "pdf":
        return "application/pdf"
    if format == "svg" or format == "svgz":
        return f + "/" + "svg"
    else:
        return f + "/" + format


def generate_csv(rows, file):
    """Crea el fichero CSV con los valores de la petición a realizar

               Parámetros:
               rows -- Lista con los parámetros para cada petición
               """
    with open(file, 'w', newline='') as csvfile:
        fieldnames = rows[0].keys()
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter="\t")

        writer.writeheader()
        for row in rows:
            writer.writerow(row)


def random_coordinates(xmin, ymin, xmax, ymax):
    """Devuelve dos coordenadas aleatorias.

       Parámetros:
       minx -- Valor mínimo para coordenadas en el eje X
       maxx -- Valor máximo para coordenadas en el eje X
       miny -- Valor mínimo para coordenadas en el eje Y
       maxy -- Valor mínimo para coordenadas en el eje Y
       """
    x, y = random.uniform(xmin, xmax), random.uniform(ymin, ymax)
    assert xmin <= x <= xmax
    assert ymin <= y <= ymax
    return x, y


def generate_random_points(number_of_rows, xmin, ymin, xmax, ymax):
    """Genera una lista de puntos (x,y) aleatorios

        parameters:
           rows -- Total de elementos a crear
    """
    rows = []
    for i in range(number_of_rows):
        row = {}
        x, y = random_coordinates(xmin, ymin, xmax, ymax)
        row["x"] = x
        row["y"] = y
        rows.append(row)
    return rows


def generate_geojson_points(results, xmin, ymin, xmax, ymax):
    features = []
    polygon = Polygon([[xmin, ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin], [xmin, ymin]])
    polygon = transform_coordinates(polygon)
    features.append(Feature(polygon))
    for row in results:
        point = Point(row['x'], row['y'])
        point = transform_coordinates(point)
        features.append(Feature(point))
    feature_collection = FeatureCollection(features)
    with open('myfile.geojson', 'w') as f:
        dump(feature_collection, f)


def transform_coordinates(coordinates):
    project = pyproj.Transformer.from_crs(pyproj.CRS('EPSG:32628'), pyproj.CRS('EPSG:4326'), always_xy=True).transform
    wgs84_coordinates = transform(project, coordinates)
    return wgs84_coordinates


def set_layers(json):
    layers = "show:"
    iteraciones = 0
    for row in json['layers']:
        iteraciones = iteraciones+1
        if iteraciones < len(json['layers']):
            layers = layers + str(row['id']) + ","
        elif iteraciones == len(json['layers']):
            layers = layers + str(row['id'])
    return layers
