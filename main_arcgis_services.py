import argparse
import random
import requests
import yaml
from utils.core import generate_random_points, generate_csv, calculate_extension, generate_geojson_points, set_layers
f = "image"


def main():
    args = extract_params()
    config = get_configuration_data()
    results_extension = generate_results_extension(args, config)
    random.shuffle(results_extension)
    generate_csv(results_extension, config['output'])


def extract_params():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--service-config", dest="service_config_path", default="service.yml")
    parser.add_argument("--username", type=str,
                        help="Username for login")
    parser.add_argument("--password", type=str,
                        help="Password for login")
    parser.add_argument("--xmin", type=float, default=0,
                        help="Minimum coordinate for X")
    parser.add_argument("--ymin", type=float, default=0,
                        help="Minimum coordinate for y")
    parser.add_argument("--xmax", type=float, default=0,
                        help="Maximum coordinate for X")
    parser.add_argument("--ymax", type=float, default=0,
                        help="Maximum coordinate for y")
    parser.add_argument("--bboxsr", type=int, default=0,
                        help="bbox spatial reference")
    parser.add_argument("--imagesr", type=int, default=0,
                        help="image spatial reference")
    parser.add_argument("--format", default='',
                        help="Image format")
    parser.add_argument("--layers", type=str, default='',
                        help="Layers to show")
    return parser.parse_args()


def get_configuration_data():
    with open('config.yml', 'r') as file_yml:
        config = yaml.safe_load(file_yml.read())
        return config


def generate_results_extension(args, config):
    results_extension = []
    for service in config['services']:
        response = get_response(args, config)
        r = make_request(response, service, config)
        json = r.json()
        service_data = get_service_data(args, json)
        results = generate_random_points(service['elements'], service_data['xmin'], service_data['ymin'],
                                         service_data['xmax'], service_data['ymax'])
        generate_geojson_points(results, service_data['xmin'], service_data['ymin'],
                                service_data['xmax'], service_data['ymax'])
        results_extension_service = calculate_extension(results, config['parameters']['scales'].split(','),
                                                        config['parameters']['imageDPI'].split(','),
                                                        service_data['bboxsr'], service_data['imagesr'],
                                                        config['parameters']['image_size'].split(','),
                                                        service_data['format'].split(','),
                                                        config['parameters']['transparent'].split(','),
                                                        service_data['layers'], f, service['url'])
        results_extension.extend(results_extension_service)
    return results_extension


def get_response(args, config):
    token_endpoint = config['parameters']['endpoint']
    payload = {'username': args.username, 'password': args.password, 'client': config['parameters']['client'],
               'referer': config['parameters']['referer'], 'expiration': config['parameters']['expiration'],
               'f': 'pjson'}
    response = request_for_get_token(token_endpoint, payload)
    return response


def request_for_get_token(token_endpoint, payload):
    try:
        result = requests.post(token_endpoint, data=payload).json()
        return result
    except requests.exceptions.HTTPError as err:
        raise SystemExit(err)


def get_service_data(args, json):
    service_data = {}
    if args.bboxsr == 0:
        service_data['bboxsr'] = json['spatialReference']['wkid']
    else:
        service_data['bboxsr'] = args.bboxsr
    if args.imagesr == 0:
        service_data['imagesr'] = json['spatialReference']['wkid']
    else:
        service_data['imagesr'] = args.imagesr
    if args.xmin == 0:
        service_data['xmin'] = json['fullExtent']['xmin']
    else:
        service_data['xmin'] = args.xmin
    if args.ymin == 0:
        service_data['ymin'] = json['fullExtent']['ymin']
    else:
        service_data['ymin'] = args.ymin
    if args.xmax == 0:
        service_data['xmax'] = json['fullExtent']['xmax']
    else:
        service_data['xmax'] = args.xmax
    if args.ymax == 0:
        service_data['ymax'] = json['fullExtent']['ymax']
    else:
        service_data['ymax'] = args.ymax
    if args.format == '':
        service_data['format'] = json['supportedImageFormatTypes'].swapcase()
    else:
        service_data['format'] = args.format
    if args.layers == '':
        service_data['layers'] = set_layers(json)
    else:
        service_data['layers'] = args.layers
    return service_data


def make_request(response, service, config):
    try:
        params = {'f': 'pjson', 'token': response['token']}
        result = requests.get(
            config['parameters']['servername'] + service['url'],
            params=params)
        return result
    except KeyError as e:
        print(response['error']['message'] + " " + response['error']['mdetails'])
    except requests.exceptions.RequestException as e:
        print(e.response.text)


main()
